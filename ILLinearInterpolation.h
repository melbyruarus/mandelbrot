//
//  ILLinearInterpolation.h
//  Mandelbrot
//
//  Created by Melby Ruarus on 14/03/10.
//  Copyright 2010 Apple Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface ILLinearInterpolation : NSObject {
	CGFloat ** internalBuffer;
	int count;
}

- (id)initWithColors:(CGFloat[])col count:(int)num;
- (void)getRed:(CGFloat *)red green:(CGFloat *)green blue:(CGFloat *)blue atPoint:(CGFloat)point withFactor:(CGFloat)factor;

@end
