//
//  ILLinearInterpolation.m
//  Mandelbrot
//
//  Created by Melby Ruarus on 14/03/10.
//  Copyright 2010 Apple Inc. All rights reserved.
//

#import "ILLinearInterpolation.h"


@implementation ILLinearInterpolation

- (id)initWithColors:(CGFloat[])col count:(int)num {
	if((self = [super init])) {
		count = num;
		internalBuffer = malloc(sizeof(CGFloat *) * count);
		
		int n;
		for(n=0;n<count;n++) {
			internalBuffer[n] = malloc(sizeof(CGFloat) * 3);
			internalBuffer[n][0] = col[n*3+0];
			internalBuffer[n][1] = col[n*3+1];
			internalBuffer[n][2] = col[n*3+2];
		}
	}
	
	return self;
}

- (void)getRed:(CGFloat *)red green:(CGFloat *)green blue:(CGFloat *)blue atPoint:(CGFloat)point withFactor:(CGFloat)factor {
	if(!(point < CGFLOAT_MAX && point > -CGFLOAT_MAX)) {
		point = 1;
	}
	
	if(point <= 0.0) {
		*red = internalBuffer[0][0] * factor;
		*green = internalBuffer[0][1] * factor;
		*blue = internalBuffer[0][2] * factor;
	}
	else if(point >= 1.0) {
		*red = internalBuffer[count-1][0] * factor;
		*green = internalBuffer[count-1][1] * factor;
		*blue = internalBuffer[count-1][2] * factor;
	}
	else {
		CGFloat range = (point*(count-1) - floor(point*(count-1)));
		CGFloat li = 1 - range;
		CGFloat ri = range;
		int n = (int)((count-1)*point);

		*red = (internalBuffer[n][0]*li + internalBuffer[n+1][0]*ri) * factor;
		*green = (internalBuffer[n][1]*li + internalBuffer[n+1][1]*ri) * factor;
		*blue = (internalBuffer[n][2]*li + internalBuffer[n+1][2]*ri) * factor;
	}
}

- (void)dealloc {
	int n;
	for(n=0;n<count;n++) {
		free(internalBuffer[n]);
	}
	free(internalBuffer);
	
	[super dealloc];
}

@end
