//
//  ILOpenCLBuffer.h
//  Mandelbrot
//
//  Created by Melby Ruarus on 21/03/10.
//  Copyright 2010 Apple Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import <OpenCL/OpenCL.h>


@class ILOpenCLContext;

@interface ILOpenCLBuffer : NSObject {
	ILOpenCLContext * context;
	cl_mem buffer;
}

- (id)initWithContext:(ILOpenCLContext *)con flags:(cl_mem_flags)flags size:(size_t)size;
- (void)writeToBuffer:(void *)pointer size:(size_t)size;
- (void)readOutBuffer:(void *)pointer size:(size_t)size;
- (void *)pointer;
- (size_t)size;

@property (readonly) ILOpenCLContext * context;
@property (readonly) cl_mem buffer;
@end
