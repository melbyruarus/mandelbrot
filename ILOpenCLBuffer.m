//
//  ILOpenCLBuffer.m
//  Mandelbrot
//
//  Created by Melby Ruarus on 21/03/10.
//  Copyright 2010 Apple Inc. All rights reserved.
//

#import "ILOpenCLBuffer.h"

#import "ILOpenCLContext.h"


@implementation ILOpenCLBuffer

- (id)initWithContext:(ILOpenCLContext *)con flags:(cl_mem_flags)flags size:(size_t)size {
	int err;
	
	context = [con retain];
	
	buffer = clCreateBuffer([context context], flags, size, NULL, &err);
	if(err != CL_SUCCESS) {
		NSLog(@"Error: Failed to allocate device memory! %@", self);
		return nil;
	}
	
	return [self init];
}

- (void)writeToBuffer:(void *)pointer size:(size_t)size {
	int err;
	
	err = clEnqueueWriteBuffer([context commandQueue], buffer, CL_TRUE, 0, size, pointer, 0, NULL, NULL);
	if(err != CL_SUCCESS) {
		NSLog(@"Error: Failed to write to source buffer! %@", self);
		return;
	}
}

- (void)readOutBuffer:(void *)pointer size:(size_t)size {
	int err;
	
	err = clEnqueueReadBuffer([context commandQueue], buffer, CL_TRUE, 0, size, pointer, 0, NULL, NULL );  
	if (err != CL_SUCCESS) {
		NSLog(@"Error: Failed to read from source buffer! %d: %@", err, self);
		return;
	}
}

- (void *)pointer {
	return &buffer;
}

- (size_t)size {
	return sizeof(cl_mem);
}

- (void)dealloc {
	[context release];
	clReleaseMemObject(buffer);
	
	[super dealloc];
}

@synthesize context;
@synthesize buffer;
@end
