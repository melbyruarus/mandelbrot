//
//  ILOpenCLContext.h
//  Mandelbrot
//
//  Created by Melby Ruarus on 20/03/10.
//  Copyright 2010 Apple Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import <OpenCL/OpenCL.h>


@interface ILOpenCLContext : NSObject {
	cl_device_id deviceId;
	cl_context context;
	cl_device_type deviceType;
	cl_command_queue commandQueue;
}

- (id)initWithDeviceType:(cl_device_type)type;

@property (readonly) cl_device_id deviceId;
@property (readonly) cl_context context;
@property (readonly) cl_device_type deviceType;
@property (readonly) cl_command_queue commandQueue;
@end
