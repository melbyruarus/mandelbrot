//
//  ILOpenCLContext.m
//  Mandelbrot
//
//  Created by Melby Ruarus on 20/03/10.
//  Copyright 2010 Apple Inc. All rights reserved.
//

#import "ILOpenCLContext.h"


@implementation ILOpenCLContext

- (id)initWithDeviceType:(cl_device_type)type {
	deviceType = type;
	
	int err;
	
	err = clGetDeviceIDs(NULL, deviceType, 1, &deviceId, NULL);
    if(err != CL_SUCCESS) {
        NSLog(@"Error: Failed to create a device group! %@", self);
		
		return nil;
    }
	
	context = clCreateContext(0, 1, &deviceId, NULL, NULL, &err);
    if(err != CL_SUCCESS) {
        NSLog(@"Error: Failed to create a compute context! %@", self);
        return nil;
    }
	
	commandQueue = clCreateCommandQueue(context, deviceId, 0, &err);
    if(err != CL_SUCCESS) {
        NSLog(@"Error: Failed to create a command commands! %@", self);
        return nil;
    }
	
	return [self init];
}

- (void)dealloc {
	clReleaseCommandQueue(commandQueue);
    clReleaseContext(context);
	
	[super dealloc];
}

@synthesize deviceId;
@synthesize context;
@synthesize deviceType;
@synthesize commandQueue;
@end
