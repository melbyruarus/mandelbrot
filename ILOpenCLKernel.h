//
//  ILOpenCLKernel.h
//  Mandelbrot
//
//  Created by Melby Ruarus on 20/03/10.
//  Copyright 2010 Apple Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import <OpenCL/OpenCL.h>


@class ILOpenCLProgram;
@class ILOpenCLBuffer;

@interface ILOpenCLKernel : NSObject {
	ILOpenCLProgram * program;
	cl_kernel kernel;
	NSString * kernelName;
	size_t localWorkGroupSize;
}

- (id)initWithProgram:(ILOpenCLProgram *)prog andKernelName:(NSString *)name;
- (void)setArgument:(int)index size:(size_t)size pointer:(void *)pointer;
- (void)setArgument:(int)index asBuffer:(ILOpenCLBuffer *)buffer;
- (void)runKernelWithGlobalWorkSize:(size_t)global localWorkSize:(size_t)local;
- (void)waitForKernelsToFinish;
// TODO: a function to figure out optimium local group size for a global group

@property (readonly) ILOpenCLProgram * program;
@property (readonly) cl_kernel kernel;
@property (readonly) NSString * kernelName;
@property (readonly) size_t localWorkGroupSize;
@end
