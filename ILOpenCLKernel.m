//
//  ILOpenCLKernel.m
//  Mandelbrot
//
//  Created by Melby Ruarus on 20/03/10.
//  Copyright 2010 Apple Inc. All rights reserved.
//

#import "ILOpenCLKernel.h"

#import "ILOpenCLProgram.h"
#import "ILOpenCLBuffer.h"
#import "ILOpenCLContext.h"


@implementation ILOpenCLKernel

- (id)initWithProgram:(ILOpenCLProgram *)prog andKernelName:(NSString *)name {
	if(![prog compiled]) {
		NSLog(@"Error: Failed to create compute kernel, program source not compiled! %@", self);
		return nil;
	}
	
	int err;
	
	program = [prog retain];
	kernelName = [name retain];
	
	char * cName = malloc(sizeof(char) * [kernelName lengthOfBytesUsingEncoding:NSUTF8StringEncoding]);
	[kernelName getCString:cName maxLength:1000000 encoding:NSUTF8StringEncoding];
		
	kernel = clCreateKernel([program program], cName, &err);
    if(!kernel || err != CL_SUCCESS) {
        NSLog(@"Error: Failed to create compute kernel! %d: %@", err, self); // TODO: print more error codes, maybe have a translation?
        return nil;
    }
	
	err = clGetKernelWorkGroupInfo(kernel, [[program context] deviceId], CL_KERNEL_WORK_GROUP_SIZE, sizeof(localWorkGroupSize), &localWorkGroupSize, NULL);
	if(err != CL_SUCCESS) {
		NSLog(@"Error: Failed to retrieve kernel work group info! %d: %@", err, self);
		return nil;
	}
	
	return [self init];
}

- (void)setArgument:(int)index size:(size_t)size pointer:(void *)pointer {
	int err;
	
	err = clSetKernelArg(kernel, index, size, pointer);
	
	if(err != CL_SUCCESS) {						// TODO: some of these should be replaced with NSExceptions
		NSLog(@"Error: Failed to set kernel argument %i! %d: %@", index, err, self);
		return;
	}
}

- (void)setArgument:(int)index asBuffer:(ILOpenCLBuffer *)buffer {
	[self setArgument:index size:[buffer size] pointer:[buffer pointer]];
}

- (void)runKernelWithGlobalWorkSize:(size_t)global localWorkSize:(size_t)local {
	int err;
		
	err = clEnqueueNDRangeKernel([[program context] commandQueue], kernel, 1, NULL, &global, NULL, 0, NULL, NULL);
	if(err != CL_SUCCESS) {
		NSLog(@"Error: Failed to execute kernel! %i: %@", err, self);
		return;
	}
}

- (void)waitForKernelsToFinish {
	clFinish([[program context] commandQueue]);
}

- (void)dealloc {
	[program release];
	[kernelName release];
	
	clReleaseKernel(kernel);
	
	[super dealloc];
}

@synthesize program;
@synthesize kernel;
@synthesize kernelName;
@synthesize localWorkGroupSize;
@end
