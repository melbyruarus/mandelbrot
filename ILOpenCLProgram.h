//
//  ILOpenCLProgram.h
//  Mandelbrot
//
//  Created by Melby Ruarus on 20/03/10.
//  Copyright 2010 Apple Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import <OpenCL/OpenCL.h>


@class ILOpenCLContext;

@interface ILOpenCLProgram : NSObject {
	ILOpenCLContext * context;
	cl_program program;
	NSString * sourceCode;
	BOOL compiled;
}

- (id)initWithContext:(ILOpenCLContext *)con andSource:(NSString *)source;
- (id)initWithContext:(ILOpenCLContext *)con andSourceFile:(NSString *)path;
- (void)compile;

@property (readonly) cl_program program;
@property (readonly) NSString * sourceCode;
@property (readonly) BOOL compiled;
@property (readonly) ILOpenCLContext * context;
@end
