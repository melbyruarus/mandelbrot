//
//  ILOpenCLProgram.m
//  Mandelbrot
//
//  Created by Melby Ruarus on 20/03/10.
//  Copyright 2010 Apple Inc. All rights reserved.
//

#import "ILOpenCLProgram.h"

#import "ILOpenCLContext.h"


@implementation ILOpenCLProgram

- (id)initWithContext:(ILOpenCLContext *)con andSource:(NSString *)source {
	if(!source) {
		NSLog(@"Error: computer program source nil! %@", self);
		return nil;
	}
	
	context = [con retain];
	sourceCode = [source retain];
	
	int err;
	
	char * cSource = malloc(sizeof(char) * [sourceCode lengthOfBytesUsingEncoding:NSUTF8StringEncoding]);
	[source getCString:cSource maxLength:1000000 encoding:NSUTF8StringEncoding];
		
	program = clCreateProgramWithSource([context context], 1, (const char **) &cSource, NULL, &err);
	free(cSource);
    if(err != CL_SUCCESS) {
        NSLog(@"Error: Failed to create compute program! %@", self);
        return nil;
    }
	
	compiled = NO;
	
	return [self init];
}

- (id)initWithContext:(ILOpenCLContext *)con andSourceFile:(NSString *)path {
	NSError * error = nil;
	
	NSString * tmp = [NSString stringWithContentsOfFile:path
											   encoding:NSUTF8StringEncoding
												  error:&error];
	
	if(error) {
		NSLog(@"Error: Failed to load source off disk! %@", error);
		
		return nil;
	}
	
	return [self initWithContext:con andSource:tmp];
}

- (void)compile {
	if(!compiled) {
		int err;
		
		err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
		if(err != CL_SUCCESS) {
			size_t len;
			char buffer[2048];
			
			NSLog(@"Error: Failed to build program executable! %@", self);
			clGetProgramBuildInfo(program, [context deviceId], CL_PROGRAM_BUILD_LOG, sizeof(buffer), buffer, &len);
			printf("%s\n", buffer);
			return;
		}
	}
	
	compiled = YES;
}

- (void)dealloc {
	[context release];
	[sourceCode release];
	
	clReleaseProgram(program);
	
	[super dealloc];
}

@synthesize program;
@synthesize sourceCode;
@synthesize compiled;
@synthesize context;
@end
