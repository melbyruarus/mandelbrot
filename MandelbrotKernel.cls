__kernel void mandelbrot(__global char4 *output,
						 const int4 pxyMaxMaxColor,
						 const float4 centerZoomBail) {
	
	int id = get_global_id(0);
	
	int4 pixel = (int4)(id % pxyMaxMaxColor.s1, id / pxyMaxMaxColor.s1, 0, 0);
	
	// --------------------- For CPU ---------------------
	
//	float x1 = (((-pixel.x*2.0f/pxyMaxMaxColor.s0)+1)*(1.5f/centerZoomBail.s2))-centerZoomBail.x;
//	float y1 = ((( pixel.y*2.0f/pxyMaxMaxColor.s1)-1)*(1.5f/centerZoomBail.s2))-centerZoomBail.y;
//	
//	float x0 = 0;
//	float y0 = 0;
//	int iteration = 0;
//	while((x0*x0 + y0*y0 <= centerZoomBail.s3) && (iteration < pxyMaxMaxColor.s2)) {
//		float xtmp = x0*x0 - y0*y0 - x1;
//		y0 = 2*x0*y0 + y1;
//		
//		x0 = xtmp;
//		
//		iteration++;
//	}
	
	// --------------------- For GPU ---------------------
	
	float4 normalizedPixel = convert_float4(pixel) / convert_float4(pxyMaxMaxColor);
	float4 shiftedPixel = normalizedPixel * (float4)(-2, 2, 0, 0) + (float4)(1, -1, 0, 0);
	float scaleAmount = 1.5/centerZoomBail.s2;
	float4 scaledPosition = shiftedPixel * scaleAmount;
	float4 startPosition = scaledPosition - centerZoomBail;
	
	float4 currentPosition = (float4)(0);
	int iteration = 0;
	while(iteration < pxyMaxMaxColor.s2) {
		float4 dist = currentPosition * currentPosition;
		if(dist.x + dist.y >= centerZoomBail.s3) {
			break;
		}
		
		currentPosition = (float4)(dist.x - dist.y - startPosition.x,
								   2 * currentPosition.x * currentPosition.y + startPosition.y,
								   0,
								   0);
		
		iteration++;
	}
	
	output[id].r = output[id].g = output[id].b = sqrt((float)iteration/pxyMaxMaxColor.s2)*pxyMaxMaxColor.s3;
	output[id].a = 255;
}