//
//  MandelbrotRenderer.h
//  Mandelbrot
//
//  Created by Melby Ruarus on 13/03/10.
//  Copyright 2010 Apple Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface MandelbrotRenderer : NSObject {
	NSBitmapImageRep * cachedImage;
	BOOL cached;
	CGFloat pixelsWide;
	CGFloat pixelsHigh;
	CGFloat centerX;
	CGFloat centerY;
	CGFloat zoom;
	
	CGFloat timeToCalculate;
	CGFloat timeToRender;
}

- (id)initWithRenderer:(MandelbrotRenderer *)renderer;
- (id)initWithView:(NSView *)view;
- (void)render:(NSBitmapImageRep *)image; // override to draw
- (void)clickedOnPixel:(NSPoint)point;
- (NSBitmapImageRep *)newBitmap;

@property (readonly) NSBitmapImageRep * cachedImage;
@property (readonly) CGFloat pixelsWide;
@property (readonly) CGFloat pixelsHigh;
@property (nonatomic) CGFloat centerX;
@property (nonatomic) CGFloat centerY;
@property (nonatomic) CGFloat zoom;
@property CGFloat timeToCalculate;
@property CGFloat timeToRender;
@end
