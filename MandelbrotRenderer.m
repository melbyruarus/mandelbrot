//
//  MandelbrotRenderer.m
//  Mandelbrot
//
//  Created by Melby Ruarus on 13/03/10.
//  Copyright 2010 Apple Inc. All rights reserved.
//

#import "MandelbrotRenderer.h"


@implementation MandelbrotRenderer

- (id)initWithRenderer:(MandelbrotRenderer *)renderer {
	pixelsWide = renderer.pixelsWide;
	pixelsHigh = renderer.pixelsHigh;
	
	centerX = renderer.centerX;
	centerY = renderer.centerY;
	
	zoom = renderer.zoom;
	
	return [self init];
}

- (id)initWithView:(NSView *)view {
	pixelsWide = view.bounds.size.width;
	pixelsHigh = view.bounds.size.height;
	
	centerX = -0.5;
	centerY = 0;
	
	zoom = 1;
	
	return [self init];
}

- (id)init {
	cachedImage = [self newBitmap];
	cached = NO;
	
	timeToCalculate = -1;
	timeToRender = -1;
	
	if(![super init]) {
		[self dealloc];
		return nil;
	}
	
	return self;
}

- (NSBitmapImageRep *)cachedImage {
	if(!cached) {
		[self render:cachedImage];
		
		cached = YES;
	}
	
	return cachedImage;
}

- (void)render:(NSBitmapImageRep *)image {
}

- (void)setCenterX:(CGFloat)x {
	centerX = x;
	
	cached = NO;
}

- (void)setCenterY:(CGFloat)y {
	centerY = y;
	
	cached = NO;
}

- (void)setZoom:(CGFloat)z {
	zoom = z;
	
	cached = NO;
}

- (void)clickedOnPixel:(NSPoint)point {
	CGFloat x = point.x;
	CGFloat y = point.y;
	
	CGFloat ox = (((x*2/[self pixelsWide])-1)*(1.5/zoom))+centerX;
	CGFloat oy = (((y*2/[self pixelsHigh])-1)*(1.5/zoom))+centerY;
	
	[self setCenterX:ox];
	[self setCenterY:oy];
	
	[self setZoom:zoom*2];
}

- (void)dealloc {
	[cachedImage release];
	
	[super dealloc];
}

- (NSBitmapImageRep *)newBitmap {
	return nil;
}

@synthesize pixelsWide;
@synthesize pixelsHigh;
@synthesize centerX;
@synthesize centerY;
@synthesize zoom;
@synthesize timeToCalculate;
@synthesize timeToRender;
@end
