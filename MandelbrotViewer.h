//
//  MandelbrotViewer.h
//  Mandelbrot
//
//  Created by Melby Ruarus on 13/03/10.
//  Copyright 2010 Apple Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "MandelbrotRenderer.h"

@interface MandelbrotViewer : NSView {
	MandelbrotRenderer * currentRenderer;
	NSString * renderer;
	NSArray * allRenderers;
	NSString * calcTime;
	NSString * renderTime;
	NSString * center;
	NSString * zoom;
	IBOutlet id realCenter;
	IBOutlet id imaginaryCenter;
	IBOutlet id zoomLevel;
}

- (IBAction)zoomTo:(id)sender;

@property (retain, nonatomic) NSString * renderer;
@property (readonly) NSArray * allRenderers;
@property (retain) NSString * calcTime;
@property (retain) NSString * renderTime;
@property (retain) NSString * center;
@property (retain) NSString * zoom;
@end
