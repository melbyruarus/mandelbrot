//
//  MandelbrotViewer.m
//  Mandelbrot
//
//  Created by Melby Ruarus on 13/03/10.
//  Copyright 2010 Apple Inc. All rights reserved.
//

#import "MandelbrotViewer.h"

#import "SimpleMandelbrotRenderer.h"


@implementation MandelbrotViewer

- (id)initWithFrame:(NSRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
		allRenderers = @[@"SimpleMandelbrotRenderer", @"SimpleMandelbrotRendererBW", @"OpenCLMandelbrotRendererCPU", @"OpenCLMandelbrotRendererGPU"];
		
		currentRenderer = nil;
        [self setRenderer:@"SimpleMandelbrotRenderer"];
		[self setCenter:@"-"];
		[self setZoom:@"-"];
    }
    return self;
}

- (void)mouseUp:(NSEvent *)theEvent {
	[currentRenderer clickedOnPixel:[theEvent locationInWindow]];
	
	[self setNeedsDisplay:YES];
}

- (void)setRenderer:(NSString *)rend {
	[renderer release];
	renderer = [rend retain];
	
	MandelbrotRenderer * tmp;
	
	if(!currentRenderer) {
		tmp = [[NSClassFromString(rend) alloc] initWithView:self];
	}
	else {
		tmp = [[NSClassFromString(rend) alloc] initWithRenderer:currentRenderer];
	}
	
	[currentRenderer release];
	currentRenderer = tmp;
	
	[self setCalcTime:@"-"];
	[self setRenderTime:@"-"];
	[self setNeedsDisplay:YES];
}

- (void)drawRect:(NSRect)dirtyRect {
    [[currentRenderer cachedImage] drawInRect:[self bounds]];
	
	CGFloat cTime = [currentRenderer timeToCalculate];
	CGFloat rTime = [currentRenderer timeToRender];
	
	if(cTime >= 0) {
		[self setCalcTime:[NSString stringWithFormat:@"%0.4f", cTime]];
	}
	if(rTime >= 0) {
		[self setRenderTime:[NSString stringWithFormat:@"%0.4f", rTime]];
	}
	
	[realCenter setStringValue:[NSString stringWithFormat:@"%0.15f", [currentRenderer centerX]]];
	[imaginaryCenter setStringValue:[NSString stringWithFormat:@"%0.15f", [currentRenderer centerY]]];
	[zoomLevel setStringValue:[NSString stringWithFormat:@"%0.2f", [currentRenderer zoom]]];
	[self setCenter:[NSString stringWithFormat:@"%0.15f%s%0.15fi", [currentRenderer centerX], [currentRenderer centerY]<0?"":"+", [currentRenderer centerY]]];
	[self setZoom:[NSString stringWithFormat:@"%0.2fx", [currentRenderer zoom]]];
}

- (IBAction)zoomTo:(id)sender {
	[currentRenderer setCenterX:[realCenter doubleValue]];
	[currentRenderer setCenterY:[imaginaryCenter doubleValue]];
	[currentRenderer setZoom:[zoomLevel doubleValue]];
	
	[self setNeedsDisplay:YES];
}

- (void)dealloc {
	[self setRenderer:nil];
	[allRenderers release];
	[calcTime release];
	[renderTime release];

	[super dealloc];
}

@synthesize renderer;
@synthesize allRenderers;
@synthesize calcTime;
@synthesize renderTime;
@synthesize center;
@synthesize zoom;
@end
