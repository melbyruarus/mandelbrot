//
//  OpenCLMandelbrotRenderer.h
//  Mandelbrot
//
//  Created by Melby Ruarus on 14/03/10.
//  Copyright 2010 Apple Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "MandelbrotRenderer.h"
#import <OpenCL/OpenCL.h>


@class ILOpenCLContext;
@class ILOpenCLProgram;
@class ILOpenCLKernel;
@class ILOpenCLBuffer;

@interface OpenCLMandelbrotRenderer : MandelbrotRenderer {
	ILOpenCLContext * context;
	ILOpenCLProgram * program;
	ILOpenCLKernel * kernel;
	ILOpenCLBuffer * output;
	int oldPixelCount;
}

- (id)initWithType:(cl_device_type)deviceType;

@end
