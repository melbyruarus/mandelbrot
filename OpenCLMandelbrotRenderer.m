//
//  OpenCLMandelbrotRenderer.m
//  Mandelbrot
//
//  Created by Melby Ruarus on 14/03/10.
//  Copyright 2010 Apple Inc. All rights reserved.
//

#import "OpenCLMandelbrotRenderer.h"

#include <OpenCL/opencl.h>


#import "ILOpenCLContext.h"
#import "ILOpenCLProgram.h"
#import "ILOpenCLKernel.h"
#import "ILOpenCLBuffer.h"


@implementation OpenCLMandelbrotRenderer

- (id)initWithType:(cl_device_type)deviceType {
	context = [[ILOpenCLContext alloc] initWithDeviceType:deviceType];
	program = [[ILOpenCLProgram alloc] initWithContext:context andSourceFile:[[NSBundle mainBundle] pathForResource:@"MandelbrotKernel" ofType:@"cls"]];
	[program compile];
	kernel = [[ILOpenCLKernel alloc] initWithProgram:program andKernelName:@"mandelbrot"];
	
	oldPixelCount = -1;
	
	return [super init];
}

- (void)dealloc {
	[output release];
	
	[kernel release];
	[program release];
	[context release];
	
	[super dealloc];
}

- (void)render:(NSBitmapImageRep *)image {
	int px = [self pixelsWide];
	int py = [self pixelsHigh];
	int pixelCount = px * py;
	int maxColor = pow(2, image.bitsPerSample)-1;
		
	if(oldPixelCount != pixelCount) {
		[output release];
		
		output = [[ILOpenCLBuffer alloc] initWithContext:context flags:CL_MEM_WRITE_ONLY size:sizeof(cl_char4) * pixelCount];
		
		oldPixelCount = pixelCount;
	}
	
	CFTimeInterval start = CFAbsoluteTimeGetCurrent(); // TODO: separate rendering
	
	cl_int4 pxyMaxMaxColor = {.s0 = px, .s1 = py, .s2 = MANDELBROT_MAX, .s3 = maxColor};
	cl_float4 centerZoomBail = {.s0 = centerX, .s1 = centerY, .s2 = zoom, .s3 = BAIL_OUT};
	
	[kernel setArgument:0 asBuffer:output];
	[kernel setArgument:1 size:sizeof(cl_int4) pointer:&pxyMaxMaxColor];
	[kernel setArgument:2 size:sizeof(cl_float4) pointer:&centerZoomBail];
	
		
	[kernel runKernelWithGlobalWorkSize:pixelCount localWorkSize:[kernel localWorkGroupSize]];
	
	[kernel waitForKernelsToFinish];
	
	[output readOutBuffer:image.bitmapData size:sizeof(cl_char4) * pixelCount];
	
	[self setTimeToCalculate:CFAbsoluteTimeGetCurrent()-start];
}

- (NSBitmapImageRep *)newBitmap {
	return [[NSBitmapImageRep alloc] initWithBitmapDataPlanes:NULL
												   pixelsWide:pixelsWide
												   pixelsHigh:pixelsHigh
												bitsPerSample:8
											  samplesPerPixel:4
													 hasAlpha:YES
													 isPlanar:NO
											   colorSpaceName:NSCalibratedRGBColorSpace
												  bytesPerRow:pixelsWide * 4
												 bitsPerPixel:4 * 8];
}

@end
