//
//  OpenCLMandelbrotRendererCPU.m
//  Mandelbrot
//
//  Created by Melby Ruarus on 13/03/13.
//
//

#import "OpenCLMandelbrotRendererCPU.h"

@implementation OpenCLMandelbrotRendererCPU

- (id)init {
	return [self initWithType:CL_DEVICE_TYPE_CPU];
}

@end
