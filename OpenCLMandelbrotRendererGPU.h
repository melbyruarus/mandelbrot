//
//  OpenCLMandelbrotRendererGPU.h
//  Mandelbrot
//
//  Created by Melby Ruarus on 13/03/13.
//
//

#import "OpenCLMandelbrotRenderer.h"

@interface OpenCLMandelbrotRendererGPU : OpenCLMandelbrotRenderer

@end
