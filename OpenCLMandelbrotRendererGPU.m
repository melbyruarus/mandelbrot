//
//  OpenCLMandelbrotRendererGPU.m
//  Mandelbrot
//
//  Created by Melby Ruarus on 13/03/13.
//
//

#import "OpenCLMandelbrotRendererGPU.h"

@implementation OpenCLMandelbrotRendererGPU

- (id)init {
	return [self initWithType:CL_DEVICE_TYPE_GPU];
}


@end
