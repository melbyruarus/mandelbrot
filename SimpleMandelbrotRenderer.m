//
//  SimpleMandelbrotRenderer.m
//  Mandelbrot
//
//  Created by Melby Ruarus on 13/03/10.
//  Copyright 2010 Apple Inc. All rights reserved.
//

#import "SimpleMandelbrotRenderer.h"

#import "ILLinearInterpolation.h"


@implementation SimpleMandelbrotRenderer

- (void)render:(NSBitmapImageRep *)image {	
	int px = [self pixelsWide];
	int py = [self pixelsHigh];
	
	int minI __block = MANDELBROT_MAX-1;
	int maxI __block = 0;
	
	float ** iters = malloc(sizeof(float *) * px);
	for(int x=0;x<px;x++) {
		iters[x] = malloc(sizeof(float) * py);
	}
	
	int maxColor = pow(2, image.bitsPerSample)-1;
	int nCores = [[NSProcessInfo processInfo] processorCount];
	CFTimeInterval startTime = CFAbsoluteTimeGetCurrent();
	
	dispatch_group_t group = dispatch_group_create();
	
	int delta = px/nCores;
	int remainder = px - delta * nCores;
	for(int start = 0, end = delta + remainder; end <= px; start += delta, end += delta) {
		dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
			for(int x=start;x<end;x++) {
				double x1 = (((-x*2.0/px)+1)*(1.5/zoom))-centerX;
				
				for(int y=0;y<py;y++) {
					double y1 = (((y*2.0/py)-1)*(1.5/zoom))-centerY;
					
					double x0 = 0;
					double y0 = 0;
					int iteration = 0;
					
					while((x0*x0 + y0*y0 <= BAIL_OUT*10) && (iteration < MANDELBROT_MAX)) {
						double xtmp = x0*x0 - y0*y0 - x1;
						y0 = 2*x0*y0 + y1;
						
						x0 = xtmp;
						
						iteration++;
					}
					
					iters[x][y] = iteration + (log(log(BAIL_OUT))-log(log(sqrt(x0*x0 + y0*y0))))/log(2);
					
					if(iters[x][y] != MANDELBROT_MAX && iters[x][y] > maxI) {
						maxI = iters[x][y];
					}
					if(iters[x][y] < minI) {
						minI = iters[x][y];
					}
				}
			}
		});
	}
	
	dispatch_group_wait(group, DISPATCH_TIME_FOREVER);
	dispatch_release(group);
	
	[self setTimeToCalculate:CFAbsoluteTimeGetCurrent()-startTime];
	startTime = CFAbsoluteTimeGetCurrent();
		
	CGFloat colors[30] = {
		1.0, 0.0, 0.0,
		1.0, 0.5, 0.0,
		1.0, 1.0, 0.0,
		0.5, 1.0, 0.0,
		0.0, 1.0, 0.0,
		0.0, 1.0, 0.5,
		0.0, 1.0, 1.0,
		0.0, 0.5, 1.0,
		0.0, 0.0, 1.0,
		0.0, 0.0, 0.0};
	
	ILLinearInterpolation * inter = [[ILLinearInterpolation alloc] initWithColors:colors count:10];
	
	for(int x=0;x<px;x++) {
		for(int y=0;y<py;y++) {
			float iter = iters[x][y];
			
			CGFloat lin;
			
			CGFloat red;
			CGFloat green;
			CGFloat blue;
			
			if(iter == MANDELBROT_MAX) {
				lin = 1.0;
			}
			else {
				lin = log(iter-minI+1)/log(maxI-minI);
			}
						
			[inter getRed:&red green:&green blue:&blue atPoint:lin withFactor:maxColor];
			
			NSUInteger pixel[3];
			
			pixel[0] = red;
			pixel[1] = green;
			pixel[2] = blue;
			
			[image setPixel:pixel atX:x y:y];
		}
	}
	
	[self setTimeToRender:CFAbsoluteTimeGetCurrent()-startTime];
	
	[inter release];
	
	for(int x=0;x<px;x++) {
		free(iters[x]);
	}
	
	free(iters);
}

- (NSBitmapImageRep *)newBitmap {
	return [[NSBitmapImageRep alloc] initWithBitmapDataPlanes:NULL
												   pixelsWide:pixelsWide
												   pixelsHigh:pixelsHigh
												bitsPerSample:8
											  samplesPerPixel:3
													 hasAlpha:NO
													 isPlanar:YES
											   colorSpaceName:NSCalibratedRGBColorSpace
												  bytesPerRow:0
												 bitsPerPixel:0];
}

@end
