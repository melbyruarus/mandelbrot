//
//  SimpleMandelbrotRenderer.m
//  Mandelbrot
//
//  Created by Melby Ruarus on 13/03/10.
//  Copyright 2010 Apple Inc. All rights reserved.
//

#import "SimpleMandelbrotRendererBW.h"

#import "ILLinearInterpolation.h"


@implementation SimpleMandelbrotRendererBW

- (void)render:(NSBitmapImageRep *)image {
	int px = [self pixelsWide];
	int py = [self pixelsHigh];
	unsigned char *data = image.bitmapData;
	
	int maxColor = pow(2, image.bitsPerSample)-1;
	int nCores = [[NSProcessInfo processInfo] processorCount];
	CFTimeInterval startTime = CFAbsoluteTimeGetCurrent();
	
	dispatch_group_t group = dispatch_group_create();
	
	int delta = px/nCores;
	int remainder = px - delta * nCores;
	for(int start = 0, end = delta + remainder; end <= px; start += delta, end += delta) {		
		dispatch_group_async(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
			for(int x=start;x<end;x++) {
				float x1 = (((-x*2.0/px)+1)*(1.5/zoom))-centerX;
				
				for(int y=0;y<py;y++) {
					float y1 = (((y*2.0/py)-1)*(1.5/zoom))-centerY;
					
					float x0 = 0;
					float y0 = 0;
					int iteration = 0;
					
					while((x0*x0 + y0*y0 <= BAIL_OUT) && (iteration < MANDELBROT_MAX)) {
						float xtmp = x0*x0 - y0*y0 - x1;
						y0 = 2*x0*y0 + y1;
						
						x0 = xtmp;
						
						iteration++;
					}
					
					data[y*px+x] = sqrt((float)iteration/MANDELBROT_MAX)*maxColor;
				}
			}
		});
	}
	
	dispatch_group_wait(group, DISPATCH_TIME_FOREVER);
	dispatch_release(group);
	
	[self setTimeToCalculate:CFAbsoluteTimeGetCurrent()-startTime];
}

- (NSBitmapImageRep *)newBitmap {
	return [[NSBitmapImageRep alloc] initWithBitmapDataPlanes:NULL
												   pixelsWide:pixelsWide
												   pixelsHigh:pixelsHigh
												bitsPerSample:8
											  samplesPerPixel:1
													 hasAlpha:NO
													 isPlanar:NO
											   colorSpaceName:NSCalibratedWhiteColorSpace
												  bytesPerRow:pixelsWide
												 bitsPerPixel:0];
}

@end
